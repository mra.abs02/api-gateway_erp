from flask import (
    Blueprint, render_template,
    request, flash, redirect,
    url_for
)
from flask import current_app as ca

from ..utils.database import *
# import config


bp = Blueprint('views', __name__)


def extract_records(req):
    """Extract info of a service from html form data

    Args:
        req: request object

    Returns:
        dict: service info
    """

    serv = {
        "NAME": req.form["apiName"].capitalize(),
        "URL": req.form["serverURL"],
        "AUTH_CHECK_API": req.form["AuthCheckAPI"],
        "API_SECRET": req.form["apiSecret"],
        "DESC": req.form["apiDesc"].capitalize(),
    }

    routes = []
    path_list = req.form.getlist("path[]")
    service_path = req.form.getlist("servicePath[]")
    method_list = req.form.getlist("Method[]")
    auth_list = req.form.getlist("authRequired[]")
    pathDesc_list = req.form.getlist("pathDesc[]")
    i = 0

    while i < len(path_list):
        tmp = {}
        tmp["servicePath"] = service_path[i].lower()
        tmp["path"] = path_list[i].lower()
        tmp["method"] = method_list[i].upper()
        tmp["authRequired"] = auth_list[i]
        tmp["pathDesc"] = pathDesc_list[i].capitalize()
        routes.append(tmp)
        i += 1
    serv["ROUTES"] = routes
    ca.logger.debug("Service: ", serv)
    return serv


@bp.route("/")
def home():
    """Home page"""

    return render_template('home.html', title='Welcome')


@bp.route("/add")
def add():
    """Adding new service page"""

    return render_template('add.html', title='Add endpoints ')


@bp.route("/add_to_db", methods=["POST"])
def add_to_db():
    """Adding the new service into DB."""

    if request.method == "POST":
        serv = extract_records(request)
        err = add_service(**serv)
        if isinstance(err, list):
            for e in err:
                flash(str(e)+" is duplicate", 'Error')
            return redirect(url_for('views.add'))
        elif err:
            flash(str(err), 'Error')
            return redirect(url_for('views.add'))
        else:
            flash("Successfully added the service.", "Success")
            return redirect(url_for('views.add'))


@bp.route("/services")
def all_services():
    """The page for list of all the available service page."""

    rec, total = get_all_services(page=0, limit=0)
    # for r in rec:
    #     print(type(r))
    return render_template("services.html", records=rec)


@bp.route("/view/<serv>")
def view_service(serv):
    """The page to view A serive info"""

    return render_template("view_service.html", records=get_serv_info(serv), prefix=config.API_PREFIX)


@bp.route("/del_emp", methods=['POST'])
def del_emp():
    """Deletes a service document/record."""

    if request.method == 'POST':
        serv_name = request.form["Delete"]
        done = del_serv_doc(serv_name)

        if done is False:
            flash("Service doesn't exist", 'error')
            return redirect(url_for('views.all_services'))

        flash("Employee record has been deleted.", 'info')
        return redirect(url_for('views.all_services'))


@bp.route("/update", methods=["POST", "GET"])
def update_page():
    """The update page"""

    if request.method == "POST":
        serv_name = request.form['Edit']
        # info = get_serv_info(serv_name)
        return render_template("update.html", records=get_serv_info(serv_name), title="Update employee details")


@bp.route("/update_serv", methods=["POST"])
def update_serv():
    """Update the service info (in the DB)."""

    if request.method == 'POST':
        org_service_name = request.form["Edit"]
        service = extract_records(request)
        err = update_record(name=org_service_name, **service)
        if isinstance(err, list):
            for e in err:
                flash(str(e)+" is duplicate", 'Error')
            # return render_template('update.html', records=request.form, title="Update the Service Details")
            return redirect(url_for('views.update_page'), code=307)
        elif err:
            flash(str(err), 'info')
            # return render_template('update.html', records=request.form, title="Update the Service Details")
            return redirect(url_for('views.update_page'), code=307)
        flash("Update Successful.", 'info')
        return redirect(url_for('views.view_service', serv=service["NAME"]),)
