# one way to fix <path:serv_path>/<path:param> is by <path:serv_path>/xxx/<path:param>
# TODO there is a bug when I copied and pasted the endpoint getfile
# TODO I have to add / after the servicepaths otherwise there is an error (In the debug mode)



from flask import Blueprint, Response, request
from flask import current_app as ca
from flask_restful import Resource
import requests
import json

from ..utils.database import *

bp = Blueprint('api', __name__)



Error = {"wrong": "Wronge Endpoint.",
         "bad": "Bad Request.", "notFoundCode": 404,
         'unauthorized': "Unauthorized Request.", "unauthCode": 401
         }

def authenticate(serv):
    """Authenticate a user/service.

    Args:
        serv: service info

    Returns:
        dict: the header info for this service
    """

    header = {
        'authorization': request.headers.get('authorization') or '',
    }

    try:
        response = requests.post(serv["auth_api"], headers=header)
    except requests.exceptions.RequestException as e:
        ca.logger.error('Authentication server connection:  %s' % str(e))
        return None

    print("Auth code:", response.status_code)
    if response.status_code == 200:
        auth_data = {
            'isAuthenticated': 'true',
            'user': response.json(),
        }
        headers = {'userMeta': json.dumps(auth_data)}

    else:
        if serv['auth_required'] == 'true':
            return None
        else:
            auth_data = {
                'isAuthenticated': 'false'
            }
            headers = {'user': json.dumps(auth_data)}

    ca.logger.debug("Authendicate header: %s" % headers)

    return headers


def compose_files():
    """Extract the files from http from data

    Returns:
        tuple: a tuple of the uploaded files.
    """

    files = []
    for f in request.files.getlist('file'):
        # print(dir(f))
        file = (f.name, (
            f.filename,
            f.read(),
            f.content_type,
            )
        )
        files.append(file)
    return files


def compose_url(url, path, postfix):
    """Compose the url for the remote services.

    Args:
        url: host address
        path: the endpoint
        postfix: parameters

    Returns:
        str: The complete url
    """

    ca.logger.debug("[COMPOSE URL] url: %s, path: %s, postfix %s" %(url, path, postfix))

    if postfix == '':
        param = path
    else:
        if path[-1] == '/':
            param = path + postfix
        else:
            param = path + '/' + postfix

    comp_url = url + param
    ca.logger.debug("[COMPOSE URL] url: %s" % comp_url)
    return comp_url


def compose_service_name(serv_path):
    """It compose the ther serve path from the requested path

    Args:
        serv_path: the service name.

    Returns:
        str: The path/endpoint for the service.
    """

    if len(serv_path) == 1 and serv_path[0] == '/':
        return serv_path
    else:
        return "/" + serv_path


class Methods(Resource):
    #TODO the arguments in the path also needs be addressed

    def get(self, serv_path, param=''):
        """The endpoint for GET methods."""

        serv = get_url_by_servPath(compose_service_name(serv_path.lower()), 'GET')

        print("[GET] serv: ", serv)

        if serv is None:
            return Error["wrong"], Error["notFoundCode"]

        headers = authenticate(serv)

        if headers is None:
            return Error['unauthorized'], Error['unauthCode']

        url = compose_url(serv["url"], serv["path"], param)
        ca.logger.info("[GET] url: %s" % url)

        args = request.args.to_dict()
        try:
            req = requests.get(url, headers=headers, params=args)

        except requests.exceptions.RequestException as e:
            ca.logger.error('Could not connect to the server:  %s' % str(e))
            return 'Could not connect to the server.', 503

        if req.status_code == 200:
            # print(req.content)
            return Response(req, headers={'Server': ''}, content_type=req.headers['Content-Type'])
        else:
            return Error["bad"], Error["notFoundCode"]

    def post(self, serv_path, param=''):
        """The endpoint for POST methods."""

        serv = get_url_by_servPath(compose_service_name(serv_path.lower()), 'POST')
        if serv is None:
            return Error["wrong"], Error["notFoundCode"]

        headers = authenticate(serv)
        if headers is None:
            return Error['unauthorized'], Error['unauthCode']


        url = compose_url(serv["url"], serv["path"], param)
        ca.logger.info("POST URL: %s" % url)

        try:
            req = requests.post(url, headers=headers, data=request.form.to_dict(), files=compose_files())

        except requests.exceptions.RequestException as e:
            ca.logger.error('Could not connect to the server:  %s' % str(e))
            return 'Could not connect to the server.', 503

        if req.status_code == 200:
            return Response(req, headers={'Server': ''}, content_type=req.headers['Content-Type'])
            # return req.content
        else:
            return Response(req, headers={'Server': ''}, content_type=req.headers['Content-Type'])

    def put(self, serv_path, param=''):
        """The endpoint for PUT methods."""

        serv = get_url_by_servPath(compose_service_name(serv_path.lower()), 'PUT')
        if serv is None:
            return Error['wrong'], Error["notFoundCode"]

        headers = authenticate(serv)
        if headers is None:
            return Error['unauthorized'], Error['unauthCode']

        url = compose_url(serv["url"], serv["path"], param)
        ca.logger.info("[PUT] url: %s" % url)

        try:
            req = requests.put(url, headers=headers, data=request.form.to_dict())

        except requests.exceptions.RequestException as e:
            ca.logger.error('Could not connect to the server:  %s' % str(e))
            return 'Could not connect to the server.', 503

        if req.status_code == 200:
            return Response(req, headers={'Server': ''}, content_type=req.headers['Content-Type'])
        else:
            return Error['bad'], Error['code']

    def delete(self, serv_path, param=''):
        """The endpoint for DELETE methods."""

        serv = get_url_by_servPath(compose_service_name(serv_path.lower()), 'DELETE')

        if serv is None:
            return Error['wrong'], Error["notFoundCode"]

        headers = authenticate(serv)
        if headers is None:
            return Error['unauthorized'], Error['unauthCode']

        args = request.args.to_dict()
        url = compose_url(serv["url"], serv["path"], param)
        ca.logger.info("[DELETE]: %s " % url)

        try:
            req = requests.delete(url, params=args)

        except requests.exceptions.RequestException as e:
            ca.logger.error('Could not connect to the server:  %s' % str(e))
            return 'Could not connect to the server.', 503

        if req.status_code == 200:
            return Response(req, headers={'Server': ''}, content_type=req.headers['Content-Type'])
        else:
            return Error['bad'], Error['code']

    def options(self, serv_path, param=''):
        """The endpoint for OPTIONS methods."""

        serv = get_url_by_servPath(compose_service_name(serv_path.lower()), 'OPTIONS')

        if serv is None:
            return Error['wrong'], Error["notFoundCode"]

        headers = authenticate(serv)
        if headers is None:
            return Error['unauthorized'], Error['unauthCode']

        url = compose_url(serv["url"], serv["path"], param)
        ca.logger.info("[OPTIONS]: %s" % url)
        args = request.args.to_dict()

        try:
            req = requests.options(url, params=args)

        except requests.exceptions.RequestException as e:
            ca.logger.error('Could not connect to the server:  %s' % str(e))
            return 'Could not connect to the server.', 503

        if req.status_code == 200:
            return Response(req, headers={'Server': ''}, content_type=req.headers['Content-Type'])
        else:
            return Error['bad'], Error['code']
