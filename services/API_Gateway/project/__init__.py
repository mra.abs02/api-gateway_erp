import uuid
from flask import Flask, jsonify
from flask_restful import Api

from .api.views import bp as feBp
from .api.api_methods import bp as apiBp
from .api.api_methods import *
import config

app = Flask(__name__)
app.secret_key = str(uuid.uuid4())

api = Api(apiBp)
api.add_resource(Methods, '/<path:serv_path>/', '/', '/<path:serv_path>/<path:param>/')

app.register_blueprint(feBp)  # front-end blueprint
app.register_blueprint(apiBp, url_prefix=config.API_PREFIX) # api blueprint

