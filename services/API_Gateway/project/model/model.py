import mongoengine as me
import config


me.connect(config.DB_NAME, host=config.DB_HOST_NAME, port=config.DB_PORT,
           username=config.DB_USER, password=config.DB_PASS)


class RouteToExpose(me.EmbeddedDocument):
    path = me.StringField(required=True)
    method = me.StringField(required=True)
    authRequired = me.StringField(required=True)
    pathDesc = me.StringField(required=True)
    servicePath = me.StringField(required=True)

class Routes(me.Document):
    NAME = me.StringField(required=True)
    URL = me.StringField(required=True)
    AUTH_CHECK_API = me.StringField()
    API_SECRET = me.StringField()
    DESC = me.StringField(required=True)
    ROUTES = me.EmbeddedDocumentListField(RouteToExpose)



