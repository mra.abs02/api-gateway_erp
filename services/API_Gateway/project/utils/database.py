import json
from ..model.model import *


def check_duplicate(servPath, method):
    """ Check for duplicate

    Args:
        servPath: the service endpoint pathe
        method: HTTP verb

    Returns:
        int: number of found item in the database.

    """
    paths = Routes.objects(ROUTES__match={'servicePath': servPath, 'method': method})
    print("DUPLICATE COUNTS: ", paths.count())
    return paths.count()


def get_url_by_servPath(servPath, method):
    """Fetch the different info of a service from DB.

    Gets the info such a authentication server, paths, routes, etc
    Args:
        servPath: the service endpoint pathe
        method: HTTP verb

    Returns:
        dict: A dictionary of different info of the service

    """
    serv = Routes.objects(ROUTES__match={'servicePath': servPath, 'method': method}).first()

    if serv is None:
        return None

    print("Getting servPath '%s', method '%s': %s "%(servPath, method, serv))

    url = {}
    url['url'] = serv['URL']
    url["auth_api"] = serv["AUTH_CHECK_API"]
    url["api_secret"] = serv["API_SECRET"]
    for r in serv.ROUTES:
        if r['servicePath'] == servPath and r['method'] == method:
            url['path'] = r['path']
            url['auth_required'] = r['authRequired']
            url['method'] = r['method']

    return url


def get_all_services(page=0, limit=0):
    """Fetch all the available services

    Args:
        page: Page number
        limit: no. of items to be fetched

    Returns:
        The services and total available services

    """

    total = Routes.objects().count()

    if page == 0 and limit == 0:
        return Routes.objects(), total

    skip = (page-1) * limit
    if skip == 0:
        skip = None
    return Routes.objects().skip(skip).limit(limit), total


def add_service(**serv):
    """ To add a new service into DB.

    Args:
        **serv (dict): The info of a service

    Returns:
        list: if there is/are duplicate values
        None: Successfully update the service
        err: if there is any error

    """
    duplicate = []
    for r in serv["ROUTES"]:
        check = check_duplicate(r['servicePath'], r["method"])
        if check > 0:
            duplicate.append(r['servicePath'])
        # print("CHECK: ", check)
    if duplicate:
        return duplicate
    # print("add_service is called....", serv)
    try:
        updated = Routes(**serv)
        updated.save()
        print("--> Added new Service ")
        return None

    # TODO for some errors it doesn't capture all errors
    except Exception as err:
        return err


def get_serv_info(serv: str):
    """Fetches a service from db.

    Args:
        serv (str): service name

    Returns:
        The mongoengine object for the service

    """
    return Routes.objects(NAME=serv).first()


def del_serv_doc(serv: str):
    """Deletes a service

    Args:
        serv (str): service name.

    Returns:
        None: if successfully removed a service.
        False: if it couldn't find the service
    """
    service = Routes.objects(NAME=serv).first()
    if service is None:
        return False
    else:
        del_emp = service.delete()
    print(del_emp)
    return del_emp


def update_record(name, **serv_details):
    """Update the records of service

    Args:
        name: The service's name
        **serv_details: services new info

    Returns:
        list: if there is/are duplicate values
        None: Successfully update the service
        err: if there is any error

    """
    print("Update name: ", name)
    r = Routes.objects(NAME=name).first()

    print("Record id: ", r.id)
    serv_details['id'] = r.id

    print("--------Check------")
    to_check = []
    temp = json.loads(r.to_json())

    duplicate = []
    temp_serv = []
    for s in serv_details["ROUTES"]:
        flag = 0
        for r in temp["ROUTES"]:
            if r['servicePath'] == s['servicePath'] and r["method"] == s["method"]:
                print("S: %s r: %s" %(s['servicePath'], r['servicePath']))
                flag += 1
                print("Count: ", flag)
                if s['servicePath'] in temp_serv:
                    duplicate.append(s['servicePath'])
                else:
                    temp_serv.append(s['servicePath'])

                print("Temp_serv", temp_serv, "dup", duplicate)
                break

        if flag == 0:
            to_check.append((s['servicePath'], s["method"]))

    for c in to_check:
        check = check_duplicate(c[0], c[1])
        if check > 0:
            duplicate.append(c[0])

    print("Duplicate: ", duplicate, to_check)

    if duplicate:
        return duplicate


    try:
        print("------ Service details ------\n")
        print(serv_details)
        print("--------------------------")
        updated = Routes(**serv_details)
        updated.save()
        print("----updated---")
        print(updated.NAME)
        return None

    except Exception as e:
        return e