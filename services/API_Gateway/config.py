import os

API_PREFIX = '/api'
DB_NAME = os.environ['MONGODB_NAME']
print("DB_NAME", DB_NAME)
DB_HOST_NAME = os.environ['MONGODB_HOSTNAME']
DB_PORT = os.environ['MONGODB_PORT']
DB_USER =os.environ['MONGODB_USER']
DB_PASS =os.environ['MONGODB_PASS']

# DB_NAME = "microservice-api"
# print("DB_NAME", DB_NAME)
# DB_HOST_NAME = "localhost"
# DB_PORT = ""
# DB_USER = ""
# DB_PASS = ""